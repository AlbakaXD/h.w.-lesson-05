#include "Arrow.h"

Arrow::Arrow(Point a, Point b, std::string type, std::string name):Shape(name, type)
{
	if (a.getX() == b.getX() && a.getY() == b.getY())
	{
		std::cerr << "ERROR: the points can't be similar" << std::endl;
		exit(1);
	}
	this->_source = a;
	this->_destination = b;
}

Arrow::~Arrow()
{
}

void Arrow::draw(const Canvas& canvas)
{
	canvas.draw_arrow(_source, _destination);
}
void Arrow::clearDraw(const Canvas& canvas)
{
	canvas.clear_arrow(_source, _destination);
}

double Arrow::getArea()
{
	return 0.0;
}

double Arrow::getPerimeter()
{
	return this->_source.distance(this->_destination);
}

void Arrow::move(Point& other)
{
	this->_source += other;
	this->_destination += other;
}


