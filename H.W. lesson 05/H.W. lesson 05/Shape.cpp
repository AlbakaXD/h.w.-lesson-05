#include "Shape.h"

Shape::Shape(const std::string& name, const std::string& type)
{
	this->_name = name;
	this->_type = type;
}

Shape::~Shape()
{
}

std::string Shape::getType()
{
	return this->_type;
}

std::string Shape::getName()
{
	return this->_name;
}

void Shape::printDetails()
{
	std::cout << "Name: " << this->getName()<<"		";
	std::cout << "Type: " << this->getType()<<"	";
	std::cout << "Area: " << this->getArea()<<"	";
	std::cout << "Perimeter: " << this->getPerimeter()<<std::endl;
}
