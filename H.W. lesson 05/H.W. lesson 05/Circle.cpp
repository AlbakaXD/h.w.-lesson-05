#include "Circle.h"


Circle::Circle(Point center, double radius, std::string type, std::string name):Shape(name, type)
{
	if (radius < 0)
	{
		std::cerr << "ERROR: radius can not be a negative number" << std::endl;
		exit(1);
	}
	this->center = center;
	this->radius = radius;
}

Circle::~Circle()
{
}

Point Circle::getCenter()
{
	return this->center;
}

double Circle::getRadius()
{
	return this->radius;
}

void Circle::draw(const Canvas& canvas)
{
	canvas.draw_circle(getCenter(), getRadius());
}

void Circle::clearDraw(const Canvas& canvas)
{
	canvas.clear_circle(getCenter(), getRadius());
}

double Circle::getArea()
{
	double area = (PI * this->radius) * (PI * this->radius);
	return area;
}

double Circle::getPerimeter()
{
	double perimeter = 2 * PI * this->radius;
	return perimeter;
}

void Circle::move(Point& other)
{
	this->center += other;
}


