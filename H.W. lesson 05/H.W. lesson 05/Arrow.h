#pragma once
#include "Shape.h"

class Arrow : public Shape
{
public:

    // Constructor
    Arrow(Point a, Point b, std::string type, std::string name);

    // Destructor
    ~Arrow();

    // Methods
    void draw(const Canvas& canvas) override;
    void clearDraw(const Canvas& canvas) override;

    // override functions if need (virtual + pure virtual)
    virtual double getArea() override;
    virtual double getPerimeter() override;
    virtual void move(Point& other);
private:
    Point _source;
    Point _destination;
};