#include "Triangle.h"

Triangle::Triangle(Point a, Point b, Point c, std::string type, std::string name):Polygon(type, name)
{
	if (a.getX() == b.getX() && a.getX() == c.getX() && b.getX() == c.getX())
	{
		std::cerr << "ERROR: points can't make a triangle";
		exit(1);
	}
	if (a.getY() == b.getY() && a.getY() == c.getY() && b.getY() == c.getY())
	{
		std::cerr << "ERROR: points can't make a triangle";
		exit(1);
	}
	this->_points.push_back(a);
	this->_points.push_back(b);
	this->_points.push_back(c);
}

Triangle::~Triangle()
{
}

void Triangle::draw(const Canvas& canvas)
{
	canvas.draw_triangle(_points[0], _points[1], _points[2]);
}

void Triangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_triangle(_points[0], _points[1], _points[2]);
}

double Triangle::getPerimeter()
{
	double perimeter = this->_points[0].distance(this->_points[1]) + this->_points[0].distance(this->_points[2]) + this->_points[1].distance(this->_points[2]);
	return perimeter;
}

double Triangle::getArea()
{
	double sum = 0, area = 0;
	int i = 0;
	for (i = 0; i < this->_points.size() - 1; i++)
	{
		sum += this->_points[i].distance(this->_points[i + 1]);
	}
	sum += this->_points[i].distance(this->_points[0]);
	area = sqrt(sum * (sum - (this->_points[0].distance(this->_points[1])) * (sum - (this->_points[0].distance(this->_points[2])) * (sum - (this->_points[1].distance(this->_points[2]))))));
	return area;
}
