#include "Rectangle.h"



myShapes::Rectangle::Rectangle(Point a, double length, double width, std::string type, std::string name):Polygon(type, name)
{
	this->_points.push_back(a);
	this->_points.push_back(Point(this->_points[0].getX(), this->_points[0].getY() - length));
	this->_points.push_back(Point(this->_points[0].getX() + width, this->_points[0].getY() - length));
	this->_points.push_back(Point(this->_points[0].getX() + width, this->_points[0].getY()));
}

myShapes::Rectangle::~Rectangle()
{
}

void myShapes::Rectangle::draw(const Canvas& canvas)
{
	canvas.draw_rectangle(_points[0], _points[1]);
}

void myShapes::Rectangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_rectangle(_points[0], _points[1]);
}

double myShapes::Rectangle::getPerimeter()
{
	double perimeter = 2 * (this->_points[0].distance(this->_points[1]) + this->_points[1].distance(this->_points[2]));
	return perimeter;
}

double myShapes::Rectangle::getArea()
{
	return this->_points[0].distance(this->_points[1]) * this->_points[1].distance(this->_points[2]);
}


