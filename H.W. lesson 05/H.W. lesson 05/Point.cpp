#include "Point.h"
#include <math.h>

Point::Point()
{
	this->x = 0;
	this->y = 0;
}

Point::Point(double x, double y)
{
	this->x = x;
	this->y = y;
}

Point::Point(const Point& other)
{
	this->x = other.x;
	this->y = other.y;
}

Point::~Point()
{
}

Point Point::operator+(const Point& other) const
{
	Point thingy = Point();
	thingy.x = this->x + other.x;
	thingy.y = this->y + other.y;
	return thingy;
}

Point& Point::operator+=(const Point& other)
{
	this->x = this->x + other.x;
	this->y = this->y + other.y;
	return *this;
}

double Point::getX() const
{
	return this->x;
}

double Point::getY() const
{
	return this->y;
}

double Point::distance(Point& other)
{
	return sqrt(pow((this->getX() - other.getX()), 2) + pow((this->getY() - other.getY()), 2));
}
