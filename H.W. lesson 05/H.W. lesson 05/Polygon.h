#pragma once

#include "Shape.h"
#include <vector>

class Polygon : public Shape
{
public:

    // Constructor
    Polygon(std::string type, std::string name);

    // Destructor
    ~Polygon();

    // Methods
    virtual double getPerimeter() = 0;
    // override functions if need (virtual + pure virtual)
    virtual void move(Point& other);
protected:
    std::vector<Point> _points;
};