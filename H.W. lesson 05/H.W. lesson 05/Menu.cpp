#include "Menu.h"

#define NEW_SHAPE 0
#define INFORMATION 1
#define DELETE 2
#define EXIT 3

#define CIRCLE 0
#define ARROW 1
#define TRIANGLE 2
#define RECTANGLE 3

#define MOVE 0
#define GET_DETAILES 1
#define CLEAR 2

Menu::Menu() 
{
	int option = 0, optionOfShape = 0, optionOfModification = 0;
	while (option != 3)
	{
		std::cout << "Enter 0 to add a new shape" << std::endl;
		std::cout << "Enter 1 to modify or get information from a current shape" << std::endl;
		std::cout << "Enter 2 to delete all of the shapes" << std::endl;
		std::cout << "Enter 3 to exit" << std::endl;

		std::cin >> option;

		if (option == NEW_SHAPE)
		{
			std::cout << "Enter 0 to add a circle" << std::endl;
			std::cout << "Enter 1 to add an arrow" << std::endl;
			std::cout << "Enter 2 to add a triangle" << std::endl;
			std::cout << "Enter 3 to add a rectangle" << std::endl;
			
			std::cin >> optionOfShape;

			if (optionOfShape == CIRCLE)
			{
				CreateCircle();
			}
			else if (optionOfShape == ARROW)
			{
				CreateArrow();
			}
			else if (optionOfShape == TRIANGLE)
			{
				CreateTriangle();
			}
			else if (optionOfShape == RECTANGLE)
			{
				CreateRectangle();
			}
			else
			{
				std::cout << "ERROR: non-valid option" << std::endl;
			}
		}
		else if (option == INFORMATION)
		{
			int i = 0;
			for (i = 0; i < this->_shapes.size(); i++)
			{
				std::cout << "Enter " << i << " for " << this->_shapes[i]->getName() << "("<<this->_shapes[i]->getType()<<")"<<std::endl;
			}
			std::cin >> optionOfModification;
			if (optionOfModification < 0 || optionOfModification >= this->_shapes.size())
			{
				std::cout << "ERROR: option non-valid"<<std::endl;
			}
			else if (this->_shapes[optionOfModification]->getType() == "Circle")
			{
				Modify(optionOfModification);
			}
			else if (this->_shapes[optionOfModification]->getType() == "Arrow")
			{
				Modify(optionOfModification);
			}
			else if (this->_shapes[optionOfModification]->getType() == "Triangle")
			{
				Modify(optionOfModification);
			}
			else if (this->_shapes[optionOfModification]->getType() == "Rectangle")
			{
				Modify(optionOfModification);
			}
		}
		else if (option == DELETE)
		{
			std::cout << "FUNCTION NOT WORKING PROPERLY!!!" << std::endl;
			deleteAll();
		}
		else if (option == EXIT)
		{
			std::cout << "Thank you for making shapes with us" << std::endl;
		}
		else
		{
			std::cout << "ERROR: non-valid option" << std::endl;
		}
		std::cout << std::endl;
		std::cout << "----------------------------" << std::endl;
		std::cout << std::endl;
	}
}

Menu::~Menu()
{
}

void Menu::CreateArrow()
{
	double x1 = 0, y1 = 0, x2 = 0, y2 = 0;
	std::string name;
	std::cout << "Enter X:" << std::endl;
	std::cin >> x1;
	std::cout << "Enter Y:" << std::endl;
	std::cin >> y1;
	std::cout << "Enter X:" << std::endl;
	std::cin >> x2;
	std::cout << "Enter Y:" << std::endl;
	std::cin >> y2;
	std::cout << "Enter the name of the new shape:" << std::endl;
	std::cin >> name;

	Point* a = new Point(x1, y1);
	Point* b = new Point(x2, y2);
	Shape* newArrow = new Arrow(*a, *b, "Arrow", name);
	this->_shapes.push_back(newArrow);
	_canvas.draw_arrow(*a, *b);

}

void Menu::CreateCircle()
{
	double x = 0, y = 0, radius = 0;
	std::string name;
	std::cout << "Enter X:" << std::endl;
	std::cin >> x;
	std::cout << "Enter Y:" << std::endl;
	std::cin >> y;
	std::cout << "Enter radius:" << std::endl;
	std::cin >> radius;
	std::cout << "Enter the name of the new shape:" << std::endl;
	std::cin >> name;

	Point* center = new Point(x, y);
	Shape *newCircle = new Circle(*center, radius, "Circle", name);
	this->_shapes.push_back(newCircle);
	_canvas.draw_circle(*center, radius);
}

void Menu::CreateTriangle()
{
	double x1 = 0, y1 = 0, x2 = 0, y2 = 0, x3 = 0, y3 = 0;
	std::string name;
	std::cout << "Enter X:" << std::endl;
	std::cin >> x1;
	std::cout << "Enter Y:" << std::endl;
	std::cin >> y1;
	std::cout << "Enter X:" << std::endl;
	std::cin >> x2;
	std::cout << "Enter Y:" << std::endl;
	std::cin >> y2;
	std::cout << "Enter X:" << std::endl;
	std::cin >> x3;
	std::cout << "Enter Y:" << std::endl;
	std::cin >> y3;
	std::cout << "Enter the name of the new shape:" << std::endl;
	std::cin >> name;

	Point* a = new Point(x1, y1);
	Point* b = new Point(x2, y2);
	Point* c = new Point(x3, y3);
	Shape *newTriangle = new Triangle(*a, *b, *c, "Triangle", name);
	this->_shapes.push_back(newTriangle);
	_canvas.draw_triangle(*a, *b, *c);
}

void Menu::CreateRectangle()
{
	double x = 0, y = 0, width = 0, length = 0;
	std::string name;
	std::cout << "Enter X:" << std::endl;
	std::cin >> x;
	std::cout << "Enter Y:" << std::endl;
	std::cin >> y;
	std::cout << "Enter length:" << std::endl;
	std::cin >> length;
	std::cout << "Enter width:" << std::endl;
	std::cin >> width;
	std::cout << "Enter the name of the new shape:" << std::endl;
	std::cin >> name;

	Point* topLeft = new Point(x, y);
	Point* bottomRight = new Point(x + width, y + length);
	Shape* newRectangle = new myShapes::Rectangle(*topLeft, length, width, "Rectangle", name);
	this->_shapes.push_back(newRectangle);
	_canvas.draw_rectangle(*topLeft, *bottomRight);
}

void Menu::Modify(int option)
{
	int optionOfFunction = 0;
	double x = 0, y = 0;
	std::cout << "Enter 0 to move the shape" << std::endl;
	std::cout << "Enter 1 to get its details" << std::endl;
	std::cout << "Enter 2 to remove the shape" << std::endl;
	std::cin >> optionOfFunction;
	if (optionOfFunction == MOVE)
	{
		std::cout << "Enter X:" << std::endl;
		std::cin >> x;
		std::cout << "Enter Y:" << std::endl;
		std::cin >> y;

		Point* newPoint = new Point(x,y);
		this->_shapes[option]->clearDraw(_canvas);
		this->_shapes[option]->move(*newPoint);
		this->_shapes[option]->draw(_canvas);
	}
	else if (optionOfFunction == GET_DETAILES)
	{
		this->_shapes[option]->printDetails();
	}
	else if (optionOfFunction == CLEAR)
	{
		std::cout << "FUNCTION NOT WORKING PROPERLY!!!" << std::endl;
		this->_shapes[option]->clearDraw(_canvas);
	}
	else
	{
		std::cout << "ERROR: non-valid option" << std::endl;
	}
}

void Menu::deleteAll()
{
	int i = 0;
	for (i = 0; i < this->_shapes.size(); i++)
	{
		this->_shapes[this->_shapes.size()-1]->clearDraw(_canvas);
		this->_shapes.pop_back();
	}
}



