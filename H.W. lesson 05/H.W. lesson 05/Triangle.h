#pragma once
#include "Polygon.h"
#include <string>

class Triangle : public Polygon
{
public:

    // Constructor
    Triangle(Point a, Point b, Point c, std::string type, std::string name);

    // Destructor
    ~Triangle();

    // Methods
    virtual void draw(const Canvas& canvas) override;
    virtual void clearDraw(const Canvas& canvas) override;

    // override functions if need (virtual + pure virtual)
    virtual double getPerimeter() override;
    virtual double getArea() override;
};