#pragma once
#include "Shape.h"
#include "Canvas.h"
#include "Arrow.h"
#include "Circle.h"
#include "Rectangle.h"
#include "Triangle.h"
#include <vector>

class Menu
{
public:

	Menu();
	~Menu();

	void CreateArrow();
	void CreateCircle();
	void CreateTriangle();
	void CreateRectangle();

	void Modify(int option);

	void deleteAll();

private: 
	Canvas _canvas;
	std::vector<Shape*> _shapes;
};

