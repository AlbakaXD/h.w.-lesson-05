#include "Polygon.h"

Polygon::Polygon(std::string type, std::string name):Shape(name, type)
{
}

Polygon::~Polygon()
{
}

void Polygon::move(Point& other)
{
	int i = 0;
	for (i = 0; i < this->_points.size(); i++)
	{
		this->_points[i] += other;
	}
}
