#pragma once

#include "Shape.h"
#include "Point.h"

#define PI 3.14

class Circle : public Shape
{
public:

    // Constructor
    Circle(Point center, double radius, std::string type, std::string name);

    // Destructor
    ~Circle();

    // Getters
    Point getCenter();
    double getRadius();

    // Methods
    void draw(const Canvas& canvas) override;
    void clearDraw(const Canvas& canvas) override;
    virtual double getArea() override;
    virtual double getPerimeter() override;
    virtual void move(Point& other) override;
    // override functions if need (virtual + pure virtual)
protected:
    Point center;
    double radius;
};